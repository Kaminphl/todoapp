import java.util.ArrayList;
import java.util.Scanner;

public class toDoApp {

	public static void main(String[] args) {
		
		ArrayList<TODOLIST> todo = new ArrayList<TODOLIST>();
		int number = 0;
		Scanner a = new Scanner(System.in);
		Scanner b = new Scanner(System.in);
		do {
			
			System.out.println("--- Welcome Todolist App ---");
			System.out.println("1) ADD TASK");
			System.out.println("2) DELETE TASK");
			System.out.println("3) EDIT TASK");
			System.out.println("4) WATCH TASK");
			System.out.println("5) Exit");
			System.out.print("Insert Number: ");
			number = a.nextInt();
			
			if(number == 1) {
				
				System.out.print("Insert Task: ");
				String Ans = b.nextLine();
				TODOLIST Add = new TODOLIST(Ans);
				todo.add(Add);
					
			}else if(number == 2){
				
				for(int i = 0;i < todo.size();i++){
					System.out.println( "|---- All Task ----|" );
					System.out.println( (i+1)+": "+todo.get(i).getTask() );
					System.out.println( "|------------------|" );
				}
				System.out.print("Insert Number To Delete Task: ");
				int choice = a.nextInt();
				todo.remove(choice-1);
				
			}else if(number == 3){
				for(int i = 0;i < todo.size();i++){
					System.out.println( "|---- All Task ----|" );
					System.out.println( (i+1)+": "+todo.get(i).getTask() );
					System.out.println( "|------------------|" );
				}
				System.out.print("Insert Number To Edit Task: ");
				int choice = a.nextInt();
				System.out.print("Insert Task: ");
				String Task = b.nextLine();
				todo.get(choice-1).setTask(Task);
				
			}else if(number == 4){
				
				if(todo.size()!=0) {
					System.out.println( "|---- All Task ----|" );
					for(int i = 0;i < todo.size();i++){
						System.out.println( (i+1)+": "+todo.get(i).getTask() );
					}
					System.out.println( "|------------------|" );
				}else {
					System.out.println("Don't Have Task ^_^");
				}
				
			}
			
		}while( number!=5 );
		
		
	}

}
